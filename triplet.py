import numpy as np
from numpy.random import default_rng

def triplet_loss(anchor, positive_indices, negative_indices, partial_loss='seuclidian', margin='softplus'):
    if partial_loss == 'seuclidian':
        positive_distance = np.linalg.norm(anchor - positive_indices, axis=1)**2
        negative_distance = np.linalg.norm(anchor - negative_indices, axis=1)**2
    elif partial_loss == 'cosine':
        positive_distance = np.asarray(positive_indices.dot(anchor))/(np.linalg.norm(anchor))
        positive_distance /= np.linalg.norm(positive_indices,axis=1)
        positive_distance = 1.0 - positive_distance
        negative_distance = np.asarray(negative_indices.dot(anchor))/(np.linalg.norm(anchor))
        negative_distance /= np.linalg.norm(negative_indices,axis=1)
        negative_distance = 1.0 - negative_distance
    loss = (positive_distance - negative_distance)
    if margin == 'maxplus':
        loss = np.maximum(0.0, 1.0 + loss)
    elif margin == 'softplus':
        loss = np.log(1 + np.exp(loss))
    return np.sum(loss)

def get_triplet_indices(embeddings_class, embeddings_full, labels_arr, anchor_id, anchor_class, num_classes):
    rng = default_rng(1)
    n_embeddings = embeddings_class.shape[0]
    # Total instances for a counter class should be at least one
    n_instances_per_class = int(n_embeddings*0.9/(num_classes-1))#max(int(embeddings_class.shape[0]*0.5/(num_classes-1)), num_classes-1)
    n_instances = n_instances_per_class * (num_classes-1)
    # Total number of instances for a class should be greater than n_instances
    assert n_instances <= n_embeddings

    # Positive labels
    # First check the position of anchor_id
    if anchor_id <= 1:
        positive_labels = rng.choice(range(2,embeddings_class.shape[0]), size=n_instances, replace=False)
    else:
        positive_labels = rng.choice((*range(0, anchor_id), *range(anchor_id+1, embeddings_class.shape[0])), size=n_instances, replace=False)
    positive_embeddings = embeddings_class[positive_labels,:]
    
    # Negative labels
    negative_embeddings = []
    for class_id in range(num_classes):
        if class_id != anchor_class:
            embedding_counter_class = embeddings_full[np.where(labels_arr == class_id)]
            negative_labels = rng.choice(range(embedding_counter_class.shape[0]), size=n_instances_per_class, replace=False)
            negative_embeddings.append(embedding_counter_class[negative_labels, :])
    negative_embeddings = np.vstack(negative_embeddings)
    return positive_embeddings, negative_embeddings

